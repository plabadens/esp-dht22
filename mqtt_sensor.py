#!/usr/bin/env micropython
"""
Publishes sensor readings to an MQTT server
"""

import dht
import machine
import utime
import network
import ubinascii
import logging
from umqtt.simple import MQTTClient

from config import CONFIG

log = logging.getLogger(__name__)


class App(object):
    def __init__(self, pin_number=CONFIG["dht_pin"]):
        pin = machine.Pin(pin_number)
        self.dht = dht.DHT22(pin)
        self.sta = network.WLAN(network.STA_IF)
        self.data = None

        id = ubinascii.hexlify(machine.unique_id())
        self.id = id

        mqtt_conf = CONFIG["mqtt"]
        self.mqtt = MQTTClient(
            id,
            mqtt_conf["server"],
            port=mqtt_conf["port"],
            user=mqtt_conf["user"],
            password=mqtt_conf["pass"])

    def net_setup(self):
        interface = self.sta

        if not machine.reset_cause() == machine.SOFT_RESET:
            interface.active(True)

            ssid = CONFIG["net"]["ssid"]
            password = CONFIG["net"]["pass"]

            log.info("Connecting to SSID: %s", ssid)
            interface.connect(ssid, password)
            while not interface.isconnected():
                utime.sleep(1)

        log.info("Connected with ip %s", interface.ifconfig()[0])

    def update(self):
        d = self.dht
        try:
            d.measure()
            temperature = d.temperature()
            humidity = d.humidity()
        except Exception as e:
            log.error("Error when reading sensor: %r", e)
        else:
            self.data = (temperature, humidity)
            log.debug("Read sensor values: %r", self.data)

    def publish(self):
        client = self.mqtt
        try:
            client.connect()
        except OSError as e:
            log.error("Could not connect to server: %r", e)
            return
        else:
            log.debug("Connected to server")

        base_topic = CONFIG["mqtt"]["base_topic"]
        data = self.data

        for i in range(len(data)):
            name = ("temperature", "humidity")[i]
            value = "%.1f" % data[i]
            topic = base_topic + name

            try:
                client.publish(topic, value)
            except OSError as e:
                log.error("Unable to publish %s: %r", name, e)
            else:
                log.info("%s: %s => %s", name, value, topic)

        try:
            client.disconnect()
        except OSError as e:
            log.error("Could not disconnect from server: %r", e)
        else:
            log.debug("Disconnected from server")

    def run(self):
        log.info("Machine id: %s", self.id)
        self.net_setup()
        interval = CONFIG["interval"]

        log.debug("Starting main loop")
        while True:
            self.update()
            self.publish()
            utime.sleep(interval)


def main():
    app = App()
    app.run()


if __name__ == "__main__":
    main()
