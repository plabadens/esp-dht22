CONFIG = {
    "dht_pin": 2,
    "interval": 30,
    "net": {
        "ssid": "my_network",
        "pass": "password"
    },
    "mqtt": {
        "base_topic": "my/fancy/topic/",
        "server": "mqtt.server.lan",
        "port": 1883,
        "user": "username",
        "pass": "password"
    }
}
